# LeasePlanAssignment

API Automation with Selenium Rest-Assured, Java 8, Maven, Serenity BDD Framework and gitlab-ci

## Requirements:
Below dependencies needs to be installed/configured
- Java 8 or higher (JAVA_HOME and PATH in environmental variables)
- Maven (M2, MAVEN_HOME and PATH in environmental variables)
- IDE
- Nice to have "Cucumber for Java" and "Gherkin" plugins installed in your IDE for better understanding and working of cucumber

## Downloading Project:
- Using git command and clone with HTTPS:
git clone https://gitlab.com/SindhujaPogakula/leaseplanproject.git

*or*

- Download the project from https://gitlab.com/SindhujaPogakula/leaseplanproject.git

## Working on feature file
- Existing feature files are currently located at "src/test/resources". Product service has two separate feature files for Positive and Negative Scenarios.
- If any new feature file needs to be added for a new service, then the file should be created in the above-mentioned path.
- If any existing feature file needs to be updated then, select the feature file.
- All the steps you write should be in Gherkin language.
- When you start typing your steps, you will get options to reuse the steps. You can choose to select the existing step or create a new step based on your scenario to be tested.
- If the resource is already declared, you can use the same by giving its constant name. Or else you can add a new resource by declaring constan


## Working on Step Definition
- All the step Definitions are currently located under "src/test/java" SearchApiPlan.actions and SearchApiPlan.stepdefinitions packages. 
- There is also a BaseActionsClass file under SearchApiPlan.actions package in which step Definitions which are common to all services are declared.
- Once you created your default step definition, put the logic you want the step definition to do.
- The hosturl is stored in serenity.config under src/test/resource which is being commonly used everywhere.


## Execution:
TestRunner file has been created under "src/test/java” in SearchApiPlan package which is responsible for executing the Scenarios in the features.





1. Run via terminal
```sh
mvn verify
```
2. Run via gitlab CI pipeline
```sh
All commands are configure in .gitlab-ci.yml file. So, you can directly run the project via gitlab ci pipeline
```


## Report:

For reference, added a Sample HTML Report generated through Serenity BDD Framework

![Summary](/uploads/e35a5c9b10fa4926bdc483769801b282/Summary.png)

![TestResult](/uploads/f596e90f1b2ffe909ac9a2bffd74e36c/TestResult.png)

## Points to remember:
- ***Scenarios covered:*** Positive and Negative scenarios mentioned in the url are covered.
- ***Reporting:*** Serenity BDD framework default reporting is used in this project.
- ***Test Report:*** Test Report is saved in location - "target/site/serenity/index.html". Test report will be visible locally only after executing the project from your terminal with command "mvn verify"
- ***CI pipeline:*** The gitlab ci pipeline has been configured.
- ***Artifacts:*** Artifacts after executing the pipeline can be downloaded from gitlab.
- ***Service Interactions:*** End to End scenarios are not covered yet as the motive was to test Get Product end points.
- ***Coverage:*** Get Method Coverage is covered with Positive and Negative Scenarios

If case of any queries/doubts, please get in touch with me at sindhuja.pogakula@gmail.com.


